const app = document.getElementById('root')

const container = document.createElement('div')
container.setAttribute('class', 'container')

app.appendChild(container)
var request = new XMLHttpRequest()

// Open a new connection, using the GET request on the URL endpoint
request.open('GET', 'https://api.openbrewerydb.org/breweries', true)

request.onload = function() {
  // Begin accessing JSON data here
  var data = JSON.parse(this.response)
  for (let index = 0; index < 10; index++) {
      const element = data[index];
      const card = document.createElement('div')
      card.setAttribute('class', 'card')
    
      const h5 = document.createElement('h5')
      h5.setAttribute('style', 'white-space: pre;');
      h5.textContent = `id: ${element.id} Name: ${element.name} `
    
      const p = document.createElement('p')
      p.setAttribute('style', 'white-space: pre;');
      p.textContent = 
      `    Type: ${element.brewery_type}   Street: ${element.street}   City: ${element.city}
    State: ${element.state}      Postal Code: ${element.postal_code}     Country: ${element.country}
    Longitude: ${element.longitude}      Latitude: ${element.latitude}       Phone: ${element.phone}
    Website: ${element.website_url}      Updated at: ${element.updated_at}`

      container.appendChild(card)

      card.appendChild(h5)
      card.appendChild(p)
  }
}

request.send()